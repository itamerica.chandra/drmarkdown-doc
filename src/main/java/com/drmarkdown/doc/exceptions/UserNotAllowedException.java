package com.drmarkdown.doc.exceptions;

/**
 * This file was created by aantonica on 20/05/2020
 */
public class UserNotAllowedException extends Exception {
    public UserNotAllowedException(String s) {
        super(s);
    }
}
